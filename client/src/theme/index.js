
const theme = {
    colors: {
        blue: {
            lightest: '#9CE5FB',
            light: '#5DD7FB',
            main: '#26BBE7',
            dark: '#27A6CC',
            darkest: '#27A6CC'
        },
        green: {
            lightest: '#B6FFEA',
            light: '#42FCCC',
            main: '#00EAB2',
            dark: '#00C491',
            darkest: '#00996D'
        },
        pink: {
            main: '#F72B52',
            darkest: '#AF0941'
        },
        red: {
            main: '#FF1D25',
            darkest: '#B20F22'
        },
        gray: {
            lightest: '#FFFFFF',
            light: '#C6C6C6',
            main: '#898989',
            dark: '#4F4F4F',
            darkest: '#282828'
        },
        danger: {
            main: '#5D5D5D'
        },

    },
    spacing: 8,
    typography: {
        fontFamily: '"BebasNeue", "Helvetica", "Arial", sans-serif"',
        fontSize:  14, // px
        fontWeightLight: 300,
        fontWeightRegular: 400,
        fontWeightMedium: 500,
    }
};

export default theme;
