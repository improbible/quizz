import React from 'react';
import { Route, Redirect } from 'react-router';

import Event from 'screens/event';
import EventManager from 'screens/event-manager';
import QuizzConfiguration from 'screens/quizz-configuration';
import QuizzStepSelection from 'screens/quizz-configuration/step-selection';
import QuizzStepEditor from 'screens/quizz-configuration/step-editor';
import ImportEvent from 'screens/import';

{/*<Route path="/import" component={ImportEvent} />,*/}
export default [
    <Redirect from="/" to="/event/033783aa-ab4c-4cf0-b7e1-e11a20136e51" />,
    <Route path="/event/:eventId" component={Event} />,
    <Route path="/event/:eventId/manage" component={EventManager} />,
    <Route path="/configuration" component={QuizzConfiguration} />,
    <Route path="/configuration/step-selection" component={QuizzStepSelection} />,
    <Route path="/configuration/step-editor" component={QuizzStepEditor} />,

];
