function repository(firebase) {
    function vote(color) {
        return firebase
            .database()
            .ref('votes')
            .set({
                color,
            });
    }

    return {
        vote,
    };
}

function connect(firebase) {
    if (!firebase) {
        throw 'firebase is not defined!';
    }

    return repository(firebase);
}

module.exports = {
    connect,
};
