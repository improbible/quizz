import OrganizationService from './organization-service';

import { firebase, clearAll } from '../firebase-mock-setup';

const organizationService = OrganizationService.connect(firebase);

afterEach(() => {
    return clearAll();
});

it('should create an organization', done => {
    organizationService
        .create({ name: 'My organization' })
        .then(organization => {
            expect(Object.keys(organization)).toHaveLength(4);
            expect(organization.id).toBeDefined();
            expect(organization.slug).toBe('my-organization');
            expect(organization.name).toBe('My organization');
            expect(organization.events).toBeDefined();

            done();
        })
        .catch(err => {
            console.log(err);
        });
});

it('should not create two organizations with the same slug', done => {
    organizationService
        .create({ name: 'My organization' })
        .then(organization => {
            return organizationService.create({
                name: 'my-organization'
            });
        })
        .catch(err => {
            expect(err).toBe('slug_exists');
            done();
        });
});
