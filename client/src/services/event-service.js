import uuid from './../utils/uuid';
import { EVENTS, ORGANIZATIONS, STEPS } from './collections';

function eventService(firebase) {
    function create({ name, organizationId, date, locationName }) {
        const organizationRef = firebase
            .database()
            .ref(`${ORGANIZATIONS}/${organizationId}`);

        return organizationRef
            .once('value')
            .then(snapshot => {
                if (!snapshot.val()) {
                    throw 'organization_not_found';
                }
            })
            .then(() => {
                const eventId = uuid();
                const event = {
                    id: eventId,
                    name,
                    organizationId,
                    date,
                    activeStep: null,
                    steps: [],
                    locationName,
                    teamRed: null,
                    teamBlue: null
                };

                const updates = {
                    [`${ORGANIZATIONS}/${organizationId}/events/${eventId}`]: true,
                    [`${EVENTS}/${eventId}`]: event
                };

                return firebase
                    .database()
                    .ref()
                    .update(updates)
                    .then(() => {
                        return event;
                    });
            });
    }

    // TODO: test me
    function addStep({eventId, step}) {
        if(!eventId) {
            throw "eventId is required"
        }
        if(!step.type) {
            throw 'Steps require a type';
        }

        const eventRef = firebase
            .database()
            .ref(`${EVENTS}/${eventId}`);

        return eventRef
            .once('value')
            .then(snapshot => {
                const event = snapshot.val();
                if (!event) {
                    throw 'event_not_found';
                }

                return event;
            })
            .then((event) => {
                const stepRefs = firebase.database().ref(STEPS);

                const updates = {}
                const keys = [];

                const key = stepRefs.push().key
                keys.push(key)
                step.id = key;

                updates[`${STEPS}/${key}`] = step

                if(!event.steps) {
                    event.steps = []
                }
                event.steps.push(step.id);

                updates[`${EVENTS}/${eventId}`] = event;

                return firebase
                    .database()
                    .ref()
                    .update(updates)
                    .then(() => {
                        return step;
                    })
            })



    }

    function gotoStep({ eventId, stepId }) {
        return firebase
            .database()
            .ref(`${EVENTS}/${eventId}`)
            .update({
                activeStep: stepId
            });
    }

    function registerVote({ eventId, team }) {
        return firebase
            .database()
            .ref(`${EVENTS}/${eventId}`)
            .transaction(event => {
                if (event) {
                    if (!event.vote) {
                        event.vote = {
                            red: 0,
                            blue: 0
                        };
                    }

                    event.vote[team]++;
                }

                return event;
            });
    }

    function questionUniqueToken({stepId}) {
        return `quizz/${stepId}`;
    }

    function answerQuestion({ stepId, index }) {

        return firebase
            .database()
            .ref(`${STEPS}/${stepId}`)
            .transaction(step => {
                if (!hasAnswered({stepId}) && step && step.type === 'quizz-question') {
                    if (!step.answers) {
                        step.answers = {};

                        step.choices.forEach((choice, idx) => {
                            step.answers[idx] = 0;
                        })
                    }

                    step.answers[index]++;
                }

                return step;
            })
            .then(() => {
                if(localStorage) {
                    localStorage.setItem(questionUniqueToken({stepId}), index);
                }
            })
    }

    function hasAnswered({stepId}) {
        if(localStorage) {
            if(localStorage.getItem(questionUniqueToken({stepId}))) {
                return true;
            }
        }

        return false
    }

    return {
        create,
        gotoStep,
        registerVote,
        addStep,
        answerQuestion,
        hasAnswered
    };
}

function connect(firebase) {
    if (!firebase) {
        throw 'firebase is not defined!';
    }

    return eventService(firebase);
}

export default {
    connect
}
