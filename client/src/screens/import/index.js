import React, { Component } from 'react';
import { connect } from 'react-redux';

import eventService from 'services/event-service';
import organizationService from 'services/organization-service';

import { firebaseConnect } from 'react-redux-firebase';

@firebaseConnect(props => [])
@connect(({ firebase }) => ({}))
class ImportEvent extends Component {
    constructor(props) {
        super(props);

        this.importEvent = this.importEvent.bind(this);
    }
    importEvent() {
        const { firebase } = this.props;


        // eventService
        //     .connect(firebase)
        //     .answerQuestion({stepId: '-LNc0xpcgACsTrghyjgy', index: 0})


        let eventId;

        organizationService
            .connect(firebase)
            .create({name: 'improbible'})
            .then((org) => {
                console.log(org)
                const event = {
                    name: 'Hackathon',
                    organizationId: org.id,
                    date: '2018-09-30T17:00:00.000Z',
                    locationName: 'Moulin à Cie'
                };

                return eventService
                    .connect(firebase)
                    .create(event)
            })
            .then((event) => {
                eventId = event.id;

                console.log(event)
                return eventService
                    .connect(firebase)
                    .addStep({eventId, step: {type: 'quizz-question', question: 'Quel est votre projet coup de coeur?', choices: ['Culture & Cie', 'EventShack', 'Qwido', 'Eurekart']}})
            })
            .then((step) => {
                return eventService
                    .connect(firebase)
                    .addStep({eventId, step: {type: 'quizz-result', questionId: step.id}})
            })

            .then(() => {
                alert('Success')
            })
            .catch((err) => {
                console.error(err);
                alert('Failed')
            })
    }

    render() {
        return <button onClick={this.importEvent}>import</button>;
    }
}

// connect to component to access props.dispatch
export default connect(state => ({}))(ImportEvent);
