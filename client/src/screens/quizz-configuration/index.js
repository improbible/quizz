import React, { Component } from 'react';

import {Layout, Header, HeaderTitle, Content } from './../../components/layout';
import FloatingButton from "../../components/button/FloatingButton";
import { withRouter } from 'react-router';

class QuizzConfiguration extends Component {
    render() {
        const {router} = this.props;

        return <Layout>
            <Header>
                <HeaderTitle>
                    Programmation
                </HeaderTitle>
            </Header>
            <Content>
                <FloatingButton onClick={() => {
                    router.push('/configuration/step-selection')
                }} />
            </Content>
        </Layout>
    }
}

export default withRouter(QuizzConfiguration)