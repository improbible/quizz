import React, { Component } from 'react';

import {Layout, Header, HeaderTitle, BackButton, Content } from './../../../components/layout';

import FloatingButton from "../../../components/button/FloatingButton";
import { withRouter } from 'react-router';

class StepSelection extends Component {
    render() {
        const { router } = this.props;

        return <Layout>
            <Header>
                <BackButton onClick={() => {
                    router.push('/configuration')
                }} />
                <HeaderTitle>
                    Nouveau module
                </HeaderTitle>
            </Header>
            <Content>
                Step selections
            </Content>
        </Layout>
    }
}

export default withRouter(StepSelection)