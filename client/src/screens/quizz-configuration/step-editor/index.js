import React, { Component } from 'react';

import {Layout, Header, HeaderTitle, BackButton, Content } from './../../../components/layout';
import {VerticalField, Input} from './../../../components/form';
import FloatingButton from "../../../components/button/FloatingButton";
import { withRouter } from 'react-router';
import TextList from "../../../components/form/TextList";

class StepEditor extends Component {
    constructor(props) {
        super(props);

        this.state = {
            texts: ['']
        }
    }
    render() {
        const {texts} = this.state;
        const {router} = this.props;

        return <Layout>
            <Header>
                <BackButton onClick={() => {
                    router.push('/configuration')
                }} />
                <HeaderTitle>
                    Nouveau module: Question
                </HeaderTitle>
            </Header>
            <Content>
                <VerticalField label="Question">
                    <Input type="textarea" />
                </VerticalField>
                <VerticalField label="Choix de réponses">
                    <TextList value={texts} onChange={(values) => {
                        this.setState({
                            texts: values
                        })
                    }}/>
                </VerticalField>
            </Content>
        </Layout>
    }
}

export default withRouter(StepEditor)