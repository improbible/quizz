import React, { Component, PropTypes } from 'react';
import './Sumary.less';

const teamShape = PropTypes.shape({
    name: PropTypes.number.isRequired,
    count: PropTypes.number.isRequired,
});

const propTypes = {
    blue: teamShape.isRequired,
    red: teamShape.isRequired,
};

const defaultProps = {};

function Bar({ name, count, color, total }) {
    const currentColor = color === 'blue' ? '#e74c3c' : '#13dcb4';
    return (
        <div>
            <div
                className="colorBar"
                style={{
                    height: count * 400 / total,
                    backgroundColor: currentColor,
                }}
            >
                <div>{count}</div>
            </div>
            <div className="colorBarTitle">{name}</div>
        </div>
    );
}

class Summary extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { blue, red } = this.props;

        return (
            <div>
                <div className="row">
                    <div className="col-xs-12 text-center">
                        <h2
                            style={{
                                fontSize: '2em',
                                marginTop: 30,
                                marginBottom: 30,
                            }}
                        >
                            Résultats du<br /> match
                        </h2>
                    </div>
                </div>
                <div className="row barChart">
                    <div className="col-xs-6 text-center">
                        <Bar
                            {...blue}
                            color="red"
                            total={blue['count'] + red['count']}
                        />
                    </div>
                    <div className="col-xs-6 text-center">
                        <Bar
                            {...red}
                            color="blue"
                            total={blue['count'] + red['count']}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

Summary.propTypes = propTypes;
Summary.defaultProps = defaultProps;

export default Summary;
