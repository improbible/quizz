
function readJsonFromEnv(envVariable) {
    const rawJson = process.env[envVariable];
    if (!rawJson) {
        throw `env variable ${envVariable} isn\'t defined!`;
    }

    return JSON.parse(rawJson);
}

function firebaseConfig() {
    const { NODE_ENV } = process.env;

    // ensure we doesn't use our prod/dev db for test
    const firebaseVar = NODE_ENV === 'production' || NODE_ENV === 'dev'
            ? 'FIREBASE_CONFIG'
            : 'FIREBASE_TEST_CONFIG';

    return readJsonFromEnv(firebaseVar);
}

module.exports = {
    firebaseConfig: firebaseConfig()
};
