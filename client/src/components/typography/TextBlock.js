import React, {PropTypes} from 'react';

import { css } from 'react-emotion';
import theme from './../../theme';

const propTypes = {
    color: PropTypes.string,
    align: PropTypes.string
}

const defaultProps = {
    align: 'center',
    color: theme.colors.gray.lightest
}

function TextBlock({children, color, align}) {
    const className = css`
        color: ${color};
        text-align: ${align};
        margin-bottom: ${theme.spacing * 4}px;
        font-size: 48px;
        font-weight: bold;
        letter-spacing: 1px;
    `;

    return (
        <div className={className}>
            { children }
        </div>
    )
}

TextBlock.propTypes = propTypes;
TextBlock.defaultProps = defaultProps;

export default TextBlock;