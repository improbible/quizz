import React from 'react';
import { Button, PinkButton, BlueButton, GreenButton, FloatingButton } from './';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

storiesOf('Buttons', module)
    .add('simple button', () => (
        <Button onClick={action('on click')}>
            awesome !
        </Button>
    ))
    .add('disabled button', () => (
        <Button disabled onClick={action('on click')}>
            awesome !
        </Button>
    ))
    .add('pink', () => (
        <PinkButton>
            Pink
        </PinkButton>
    ))
    .add('green', () => (
        <GreenButton>
            Green
        </GreenButton>
    ))
    .add('blue', () => (
        <BlueButton>
            Blue
        </BlueButton>
    ))
    .add('block', () => (
        <BlueButton block>
            Blue
        </BlueButton>
    ))
    .add('floating button', () => (
        <FloatingButton>
            +
        </FloatingButton>
    ))
