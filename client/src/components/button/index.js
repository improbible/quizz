import Button from './Button';
import FloatingButton from './FloatingButton';
import createButtonVariant from './createButtonVariant';
import theme from './../../theme';

const { colors } = theme;

const PinkButton = createButtonVariant(colors.gray.darkest, colors.pink.main, colors.pink.darkest)
const GreenButton = createButtonVariant(colors.gray.darkest, colors.green.main, colors.green.darkest)
const BlueButton = createButtonVariant(colors.gray.darkest, colors.blue.main, colors.blue.darkest)

export {
    Button,
    PinkButton,
    GreenButton,
    BlueButton,
    FloatingButton,
}