import React, {Component, PropTypes} from 'react';
import { css } from 'react-emotion';

import theme from './../../theme';

const propTypes = {
    disabled: PropTypes.boolean,
    type: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    block: PropTypes.bool,
    style: PropTypes.object
}

const defaultProps = {
    disabled: false,
    type: 'button',
    onClick: (e) => {},
    className: '',
    block: false,
    style: {}
}

class Button extends Component {
    render() {
        const {
            children,
            disabled,
            type,
            onClick,
            className,
            block,
            style
        } = this.props;

        const btnClassName = css`
            border: none;
            padding: ${theme.spacing}px ${theme.spacing * 2}px;
            min-width: 100px;
            font-weight: bold;
            font-size: 24px;
            width: ${block ? '100%' : 'auto'};
            
            :hover {
                pointer: cursor;
            }
        `;

        return (
            <button
                className={`${btnClassName} ${className}`}
                disabled={disabled}
                type={type}
                onClick={onClick}
                style={style}
            >
                { children }
            </button>
        )
    }
}

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;

export default Button;