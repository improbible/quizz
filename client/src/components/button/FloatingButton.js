import React, {Component, PropTypes} from 'react';
import { css } from 'react-emotion';

const propTypes = {
    disabled: PropTypes.boolean,
    onClick: PropTypes.func,
    bottom: PropTypes.number,
    right: PropTypes.number,
}

const defaultProps = {
    disabled: false,
    onClick: (e) => {},
    bottom: 20,
    right: 20,
}

class FloatingButton extends Component {
    render() {
        const {
            children,
            disabled,
            onClick,
            bottom,
            right
        } = this.props;

        const className = css`
            position: absolute;
            width: 40px;
            height: 40px;
            bottom: ${bottom}px;
            right: ${right}px;
            border-radius: 50%;
        `;

        return (
            <button
                className={className}
                disabled={disabled}
                type="button"
                onClick={onClick}
            >
                { children }
            </button>
        )
    }
}

FloatingButton.propTypes = propTypes;
FloatingButton.defaultProps = defaultProps;

export default FloatingButton;