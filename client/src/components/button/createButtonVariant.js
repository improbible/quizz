import React from 'react';
import {css} from 'react-emotion';

import Button from './Button';

function createButtonVariant(color, bgColor, bgColorPressed) {
    const className = css`
        color: ${color};
        background-color: ${bgColor};
        
        :active {
            background-color: ${bgColorPressed}
        }
    `

    return function WrappedButton(props) {
        return <Button {...props} className={`${className} ${props.className || ''}`}/>
    }
}


export default createButtonVariant;