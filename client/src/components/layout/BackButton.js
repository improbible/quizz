import React, {PropTypes} from 'react';
import Icon from '@mdi/react';
import { mdiKeyboardBackspace } from '@mdi/js';

import theme from "../../theme";
import { css } from 'react-emotion'

const propTypes = {
    onClick: PropTypes.func
};

const defaultProps = {
    onClick: PropTypes.func
};

function BackButton({onClick}) {
    const className = css`
        :hover {
            cursor: pointer;
        }
    `;

    return (
        <div className={className} onClick={onClick}>
            <Icon path={mdiKeyboardBackspace}
                  size={1}
                  horizontal
                  vertical
                  style={{display: 'inline-block'}}
                  color={theme.colors.gray.lightest}/>
        </div>
    );
}

BackButton.propTypes = propTypes;
BackButton.defaultProps = defaultProps;

export default BackButton;
