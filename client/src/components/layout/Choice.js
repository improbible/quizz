import React from 'react';
import styled from 'react-emotion'
import theme from "../../theme";

const Choice = styled('li')`
    width: 100%;
    font-size: 24px;
    background-color: ${ props => props.selected ? theme.colors.green.main : theme.colors.gray.lightest };
    color: ${ theme.colors.gray.darkest };
    padding: ${ theme.spacing }px;
    margin: ${ theme.spacing }px 0;
    
    :hover {
        cursor: pointer;
    }
`;

export default Choice;
