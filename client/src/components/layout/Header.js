import React from 'react';
import styled from 'react-emotion'
import theme from "../../theme";

const Header = styled('div')`
    width: 100%;
    background-color: ${props => props.bgColor ? props.bgColor : theme.colors.blue.main};
    color: ${theme.colors.gray.lightest};
    padding: ${theme.spacing}px ${theme.spacing * 2}px;
    display: flex;
    flex-direction: row;
`;

export default Header;
