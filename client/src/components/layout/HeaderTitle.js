import React from 'react';
import styled from 'react-emotion'

const HeaderTitle = styled('div')`
    font-size: 24px;
    text-transform: uppercase;
    flex-grow: 1;
    text-align: center;
    font-weight: bold;
    letter-spacing: 1px;
`;

export default HeaderTitle;