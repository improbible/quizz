import React from 'react';
import theme from './../../theme'

function PoweredByImprobible() {
    return (
        <div style={{textAlign: 'center'}}>
            <div style={{color: theme.colors.gray.lightest}}>Présenté par</div>
            <img
                src="/public/assets/logo_improbible.svg"
                alt="improbible"
                className="img img-responsive"
                style={{display: 'block',
                    'margin-left': 'auto',
                    'margin-right': 'auto',
                    'width': '50%'}}
            />
        </div>
    )
}


export default PoweredByImprobible;