import React from 'react';
import {Layout, Header, BackButton, HeaderTitle, Content, Footer} from './';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

storiesOf('Layout', module)
    .add('with header', () => (
        <Layout>
            <Header>
                <BackButton onClick={action('Back clicked!')} />
                <HeaderTitle>Header title!</HeaderTitle>
            </Header>
            <Content>
                content
            </Content>
            <Footer>
                test
            </Footer>
        </Layout>
    ))
