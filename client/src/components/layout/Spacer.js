import React, {PropTypes} from 'react';
import styled from 'react-emotion';

const propTypes = {
    size: PropTypes.number.isRequired
}

const defaultProps = {}

const Spacer = styled('div')`
    height: ${ props => props.size }px;
`

Spacer.propTypes = propTypes;
Spacer.defaultProps = defaultProps;

export default Spacer;