import React from 'react';
import styled from 'react-emotion'

const Layout = styled('div')`
    width: 100vw;
    height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
`;

export default Layout;