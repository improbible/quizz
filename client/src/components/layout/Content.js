import React from 'react';
import { css } from 'react-emotion'

import theme from './../../theme';

function Content({children}) {
    const className = css`
        padding: ${theme.spacing * 4}px ${theme.spacing * 2}px;
        flex-grow: 1;
        overflow-y: auto;
    `;

    const contentWrapperClassName = css`
        position: relative;
    `;

    return (
        <div className={`${className}`}>
            <div className={`col-xs-12 col-md-6 col-md-offset-3 ${contentWrapperClassName}`}>
                { children }
            </div>
        </div>
    )
}

export default Content;