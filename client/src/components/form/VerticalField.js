import React, {Component, PropTypes} from 'react';

import styled, { css } from 'react-emotion';
import theme from '../../theme';

const propTypes = {
    label: PropTypes.string.isRequired
}

const defaultProps = {}

const Label = styled('label')`
    font-weight: bold;
    font-size: 18px;
    margin-bottom: ${theme.spacing}px;
`

class VerticalField extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {label, children} = this.props;

        const className = css`
            :not(:first-child) {
                margin-top: ${theme.spacing * 2}px;
            }
        `

        return (
            <div className={className}>
                <div>
                    <Label>{ label }</Label>
                </div>
                <div>{ children }</div>
            </div>
        )
    }
}

VerticalField.propTypes = propTypes;
VerticalField.defaultProps = defaultProps;

export default VerticalField;