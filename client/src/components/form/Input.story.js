import React, { Component } from 'react';
import { Button, FloatingButton, VerticalField, TextList } from './';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Input from "./Input";

class StatefulInput extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: ''
        }
    }
    render() {
        return (
            <Input {...this.props} value={this.state.value} onChange={(value) => (this.setState({value}))}/>
        )
    }
}

class StatefulTextList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: ['Text #1', 'Text #2']
        }
    }
    render() {
        return (
            <TextList {...this.props} value={this.state.value} onChange={(value) => (this.setState({value}))}/>
        )
    }
}

storiesOf('Forms', module)
    .add('textbox', () => (
        <StatefulInput type="text" placeholder="Placeholder..." />
    ))
    .add('secondary style', () => (
        <StatefulInput primary={false} placeholder="Placeholder..." type="text" />
    ))
    .add('textarea', () => (
        <StatefulInput type="textarea" />
    ))
    .add('vertical field', () => (
        <VerticalField  label="Awesome label">
            <StatefulInput type="text" placeholder="Placeholder..." />
        </VerticalField>
    ))
    .add('text list', () => (
        <StatefulTextList />
    ))
