import React, {Component, PropTypes} from 'react';

import { css } from 'react-emotion';
import theme from './../../theme';

const propTypes = {
    value: PropTypes.string.required,
    onChange: PropTypes.func.required,
    placeholder: PropTypes.string,
    type: PropTypes.string,
    className: PropTypes.string,
    primary: PropTypes.boolean,
    inputProps: PropTypes.object,
    width: PropTypes.string
}

const defaultProps = {
    type: 'text',
    placeholder: '',
    primary: true,
    inputProps: {},
    width: '100%'
}

class Input extends Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(evt) {
        const {onChange} = this.props;

        onChange(evt.target.value);
    }
    render() {
        const {
            value,
            type,
            primary,
            placeholder,
            inputProps,
            children,
            width,
            className
        } = this.props;

        const textColor = primary ? theme.colors.gray.darkest : theme.colors.gray.lightest;
        const placeholderColor = primary ? theme.colors.gray.dark : theme.colors.gray.lightest;
        const bgColor = primary ? theme.colors.gray.lightest : theme.colors.blue.main;
        const borderColor = primary ? theme.colors.blue.main : theme.colors.gray.lightest;

        const rootClassName = css`
            color: ${textColor};
            background-color: ${bgColor};
            padding: 8px;
            width: ${width};
            border-style: solid;
            border-width: 3px;
            border-color: ${bgColor};
            
            ::placeholder {
                color: ${placeholderColor}
            }
            
            :focus {
                border-color: ${borderColor};
            }
        `

        const elementProps = {
            ...inputProps,
            className: `${rootClassName} ${className}`,
            type,
            placeholder,
            value,
            onChange: this.handleChange,
            onBlur: this.handleChange
        };

        if(type === 'textarea') {
            return React.createElement('textarea', elementProps, children || null)
        }

        return React.createElement('input', elementProps, children || null)
    }
}

Input.propTypes = propTypes;
Input.defaultProps = defaultProps;

export default Input;
