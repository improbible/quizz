import React, {Component, PropTypes} from 'react';
import {css} from 'react-emotion';
import {Input} from './';
import theme from './../../theme';

const propTypes = {
    value: PropTypes.arrayOf(PropTypes.string).isRequired,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
}

const defaultProps = {
    placeholder: 'Ajouter du texte...'
}

const inputClassName = css`
    margin-bottom: ${theme.spacing}px;
`

const addBtnClassName = css`
    text-align: left;
`

class TextList extends Component {
    constructor(props) {
        super(props);

        this.handleAdd = this.handleAdd.bind(this);
    }
    handleAdd() {
        const {value, onChange} = this.props;

        if(value) {
            onChange([
                ...value,
                ''
            ]);
        }
        else {
            onChange(['']);
        }
    }
    render() {
        const {value, placeholder, onChange} = this.props;

        const texts = value ? value : [];

        return (
            <div>
                {
                    texts.map((text, index) => (
                        <Input
                            primary={false}
                            className={inputClassName}
                            value={text}
                            placeholder={placeholder}
                            onBlur={(value) => {
                                const nextValues = texts.map((txt, idx) => {
                                    if(index === idx) {
                                        return value
                                    }

                                    return txt;
                                })

                                onChange(nextValues);
                            }}
                        />
                    ))
                }
                <div className={addBtnClassName} onClick={this.handleAdd}>Add</div>
            </div>
        )
    }
}

TextList.propTypes = propTypes;
TextList.defaultProps = defaultProps;

export default TextList;
