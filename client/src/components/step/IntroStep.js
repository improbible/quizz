import React, { Component, PropTypes } from 'react';

import Image from './Image';

const propTypes = {};

const defaultProps = {};

class IntroStep extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <Image src="/public/assets/storyboard/1-info-soiree.png" />;
    }
}

IntroStep.propTypes = propTypes;
IntroStep.defaultProps = defaultProps;

export default IntroStep;
