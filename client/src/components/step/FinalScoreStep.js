import React, { Component, PropTypes } from 'react';
import Image from './Image';

const propTypes = {
    category: PropTypes.number.isRequired,
    duration: PropTypes.number.isRequired,
    id: PropTypes.string.isRequired,
    nature: PropTypes.string.isRequired,
    nbOfPlayers: PropTypes.number.isRequired,
    state: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    voteDuration: PropTypes.number.isRequired,
    onSubmit: PropTypes.func.isRequired,
};

const defaultProps = {};

class FinalScoreStep extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <Image src="/public/assets/storyboard/7-resultat-match.png" />;
    }
}

FinalScoreStep.propTypes = propTypes;
FinalScoreStep.defaultProps = defaultProps;

export default FinalScoreStep;
