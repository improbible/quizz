import React, {Component} from 'react';
import theme from "../../theme";
import { Layout, Header, HeaderTitle, Content, PoweredByImprobible, Spacer } from './../layout';
import {TextBlock} from '../typography'
import Icon from '@mdi/react';
import { mdiCrown } from '@mdi/js';

class QuizzQuestionResult extends Component {
    constructor(props) {
        super(props);

        this.getWinners = this.getWinners.bind(this);
    }
    getWinners() {
        const { questionStep } = this.props;

        let best = 0;
        let winners = [];

        if(questionStep.answers) {
            questionStep.answers
                .map((count) => (parseInt(count, 10)))
                .forEach((count, index) => {
                const current = {id: index, text: questionStep.choices[index]}
                if(count > best) {
                    best = count;
                    winners = [current]
                }
                else if(count !== 0 && count === best) {
                    winners.push(current)
                }
            })
        }
        console.log(winners, questionStep.answers)

        return winners;
    }
    render() {
        const winners = this.getWinners();

        return <Layout>
            <Header bgColor={theme.colors.green.main}>
                <HeaderTitle>Résultat</HeaderTitle>
            </Header>
            <Content>
                <TextBlock>
                    ET LE GAGNANT DU PRIX COUP DE COEUR EST...
                </TextBlock>
                <div style={{textAlign: 'center'}}>
                    <Icon path={mdiCrown}
                          size={4}
                          style={{display: 'inline-block'}}
                          color={theme.colors.pink.main}/>
                </div>
                { winners.length == 0 && <TextBlock color={theme.colors.green.main} >...</TextBlock>}
                {
                    winners.map(({id, text}) => {
                        return <TextBlock key={id} color={theme.colors.green.main}>
                                {text}
                        </TextBlock>

                    })
                }
                <Spacer size={theme.spacing * 4} />
                <PoweredByImprobible />
            </Content>
        </Layout>
    }
}

export default QuizzQuestionResult;
