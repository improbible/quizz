import React, { Component, PropTypes } from 'react';
import Image from './Image';

const propTypes = {};

const defaultProps = {};

class StarStep extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <Image src="/public/assets/storyboard/8-coup-de-coeur.png" />;
    }
}

StarStep.propTypes = propTypes;
StarStep.defaultProps = defaultProps;

export default StarStep;
