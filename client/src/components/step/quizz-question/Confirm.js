import React, {Component} from 'react';

import {Layout, Header, HeaderTitle, Content, Question, Confirm} from './../../layout';
import { TextBlock } from '../../typography';
import theme from "../../../theme";
import {PinkButton, GreenButton} from "../../button";

class QuizzConfirm extends Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(choice) {
        this.setState({choice});
    }

    render() {
        const {q, onConfirm, onCancel} = this.props;

        return (<Layout>
            <Header bgColor={theme.colors.green.main}>
                <HeaderTitle>Question</HeaderTitle>
            </Header>
            <Content>
                <TextBlock align="right">Voulez-vous confirmer votre choix pour</TextBlock>
                <TextBlock color={theme.colors.green.main} align="right">
                    {q}
                </TextBlock>
                <GreenButton block={true} style={{marginTop: theme.spacing * 4, marginBottom: theme.spacing}} onClick={onConfirm}>
                    Confirmer
                </GreenButton>
                <PinkButton block={true} onClick={onCancel}>
                    Changer mon choix
                </PinkButton>
            </Content>
        </Layout>)
    }
}

export default QuizzConfirm
