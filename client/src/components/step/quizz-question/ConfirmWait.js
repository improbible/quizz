import React, {Component} from 'react';

import {Layout, Header, HeaderTitle, Content, Spacer} from './../../layout';
import theme from "../../../theme";
import TextBlock from "../../typography/TextBlock";

class QuizzConfirmWait extends Component {
    render() {
        return (<Layout>
            <Header bgColor={theme.colors.green.main}>
                <HeaderTitle>Attente insoutenable</HeaderTitle>
            </Header>
            <Content>
                <TextBlock>Merci d'avoir voté</TextBlock>
                <div style={{textAlign: 'center'}}>
                    <img
                        src="/public/assets/singe-drum-dance.gif"
                        alt="singe drum dance"
                        className="img img-responsive"
                        style={{display: 'inline-block',
                            'margin': 'auto',
                            'width': '80%'}}
                    />
                </div>
                <Spacer size={theme.spacing * 4} />
                <TextBlock>N'oubliez pas de répéter l'expérience demain!</TextBlock>
            </Content>
        </Layout>)
    }
}

export default QuizzConfirmWait
