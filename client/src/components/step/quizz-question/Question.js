import React, {Component} from 'react';

import {Layout, Header, HeaderTitle, Content, ChoiceList, Choice, PoweredByImprobible, Spacer} from './../../layout';
import {TextBlock} from '../../typography';
import theme from "../../../theme";

class QuizzQuestion extends Component {
    render() {
        const {question, choices, onSelect} = this.props;

        return (<Layout>
            <Header bgColor={theme.colors.green.main}>
                <HeaderTitle>Question</HeaderTitle>
            </Header>
            <Content>
                <TextBlock>{question}</TextBlock>
                <ChoiceList>
                    {
                        choices.map((choice) => (
                            <Choice
                                onClick={() => {
                                    onSelect(choice);
                                }}
                            >{choice.text}</Choice>
                        ))
                    }
                </ChoiceList>
                <Spacer size={theme.spacing * 4} />
                <PoweredByImprobible />
            </Content>
        </Layout>)
    }
}

export default QuizzQuestion
