import React, {Component} from 'react';

import Confirm from "./Confirm";
import Question from "./Question";
import QuizzConfirmWait from "./ConfirmWait";
import EventService from './../../../services/event-service';

class QuizzQuestion extends Component {
    constructor(props) {
        super(props);

        const {
            step,
            firebase
        } = props;

        this.state = {
            choice: null,
            confirmed: EventService.connect(firebase).hasAnswered({stepId: step.id})
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleConfirm = this.handleConfirm.bind(this);
    }

    handleChange(choice) {
        this.setState({choice});
    }

    handleConfirm() {
        const {
            firebase,
            step
        } = this.props;

        const { choice } = this.state;

        EventService
            .connect(firebase)
            .answerQuestion({stepId: step.id, index: choice.id});

        // don't wait for firebase confirmation just give instant feedback to user
        this.setState({confirmed: true});
    }

    handleCancel() {
        this.setState({choice: null});
    }

    render() {
        const {choice, confirmed} = this.state;
        const {step} = this.props;

        if(confirmed) {
            return <QuizzConfirmWait />;
        }

        return choice
            ? <Confirm
                q={choice.text}
                onConfirm={this.handleConfirm}
                onCancel={this.handleCancel}
            />
            : <Question
                choices={step.choices.map((text, index) => ({id: index, text}))}
                question={step.question}
                onSelect={this.handleChange}
            />;
    }
}

export default QuizzQuestion;
