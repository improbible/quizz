import React, { Component, PropTypes } from 'react';
import Image from './Image';

const propTypes = {};

const defaultProps = {};

class ImproRatingStep extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <Image src="/public/assets/storyboard/5-appreciation.png" />;
    }
}

ImproRatingStep.propTypes = propTypes;
ImproRatingStep.defaultProps = defaultProps;

export default ImproRatingStep;
