import React, { Component, PropTypes } from 'react';

const propTypes = {
    id: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    blue: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    isStarted: PropTypes.bool,
    locationName: PropTypes.string.isRequired,
    moderator: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    organization: PropTypes.string.isRequired,
    red: PropTypes.string.isRequired,
};

const defaultProps = {};

class Event extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {
            id,
            address,
            blue,
            date,
            isStarted,
            locationName,
            moderator,
            name,
            organization,
            red,
        } = this.props;

        return (
            <div>
                <h3>{name}</h3>
                <div>
                    <b>date</b>: <span>{date}</span>
                </div>
                {/*<b>isStarted</b>: <span>{isStarted}</span>*/}
                {/*<b>moderator</b>: <span>{moderator}</span>*/}
                <div>
                    <span>{red}</span> vs. <span>{blue}</span>
                </div>
                <div>
                    <b>Lieu</b>:{' '}
                    <span>
                        {locationName} <span>{address}</span>
                    </span>
                </div>
            </div>
        );
    }
}

Event.propTypes = propTypes;
Event.defaultProps = defaultProps;

export default Event;
