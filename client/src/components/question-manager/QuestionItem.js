import React from 'react';

import { css } from 'react-emotion';
import {sortableElement} from 'react-sortable-hoc';

function QuestionItem({ id, text }) {
    const className = css`
        padding: 4px 8px;
        margin-bottom: 4px;
        background-color: gray;
        list-style: none;
    `;

    return (
        <li className={className}>
            { text }
        </li>
    )
}

export default sortableElement(QuestionItem);