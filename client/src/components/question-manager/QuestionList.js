import React, {Component} from 'react';
import QuestionItem from "./QuestionItem";
import {SortableContainer, arrayMove} from 'react-sortable-hoc';

const SortableQuestionList = SortableContainer(({ questions }) => {
    return (
        <ul>
            {
                questions.map(({id, text}, index) => (
                    <QuestionItem key={`${id}-${index}`} id={id} text={text} index={index}/>
                ))
            }
        </ul>
    )
});

class QuestionList extends Component {
    constructor(props) {
        super(props);

        this.onSortEnd = this.onSortEnd.bind(this);
    }
    onSortEnd({oldIndex, newIndex}) {
        const {questions, onChange} = this.props;

        onChange(arrayMove(questions, oldIndex, newIndex))
    }
    render() {
        const {questions} = this.props;

        return (
            <SortableQuestionList questions={questions} onSortEnd={this.onSortEnd} />
        )
    }
}

export default QuestionList;