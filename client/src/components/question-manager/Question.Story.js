import React, {Component} from 'react';
import { QuestionList } from './';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

const questions = [
    {id: '1', text: 'Do you like ice cream?'},
    {id: '2', text: 'What\'s your favorite colour?'},
    {id: '3', text: 'What\'s your name?'},
];

class StatefulList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            questions
        }
    }
    render() {
        return (
            <QuestionList
                onChange={(questions) => {
                    this.setState({questions})
                }}
                questions={this.state.questions}
            />
        )
    }
}

storiesOf('Question Manager', module)
    .add('question list with reorder', () => (
        <QuestionList
            onChange={action('on change')}
            questions={questions}
        />
    ))
    .add('stateful list', () => (
        <StatefulList />
    ))

