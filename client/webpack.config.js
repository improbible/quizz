const webpack = require('webpack');
const path = require('path');
require('dotenv').config();

const PROD = process.env.NODE_ENV === 'production';
const TEST = process.env.NODE_ENV === 'test';
const DEV = !PROD && !TEST;
const APP_ENTRY = path.join(__dirname, 'src/app.js');

const config = {
    entry: {
        app: DEV
            ? [APP_ENTRY].concat('webpack-hot-middleware/client')
            : APP_ENTRY,
        vendors: [
            'babel-polyfill',
            'react',
            'react-dom',
            'react-redux',
            'react-redux-firebase',
            'react-router',
            'react-tap-event-plugin',
            'lodash',
            'redux',
            'redux-form',
            'redux-actions',
            'redux-saga',
            'redux-thunk'
        ],
    },
    output: {
        path: path.join(__dirname, '.dist/js'),
        publicPath: '/js/',
        filename: '[name].js',
        chunkFilename: '[name].[chunkhash].js',
    },
    devtool: PROD ? 'source-map' : 'cheap-module-eval-source-map',
    watch: !PROD,
    cache: !PROD,
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
            },
            {
                test: /\.less$/,
                use: [
                    'style-loader',
                    { loader: 'css-loader', options: { importLoaders: 1 } },
                    'less-loader',
                ],
            },
            {
                test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                loader: 'file-loader',
            },
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            __DEV__: DEV,
            __PROD__: PROD,
            __GOOGLE_ANALYTICS__: (process.env.GOOGLE_ANALYTICS) ? JSON.stringify(process.env.GOOGLE_ANALYTICS) : null,
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV),
                FIREBASE_CONFIG: JSON.stringify(process.env.FIREBASE_CONFIG || ''),
                FIREBASE_TEST_CONFIG: JSON.stringify(process.env.FIREBASE_TEST_CONFIG || '')
            },
        }),
        new webpack.optimize.CommonsChunkPlugin({
            names: ['vendors'],
        }),
    ],
    resolve: {
        modules: [
            path.join(__dirname, 'node_modules'),
            path.join(__dirname, 'src'),
        ],
    },
};

if (DEV) {
    config.plugins.push(
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
    );
}
if (PROD) {
    config.plugins.push(
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                unused: true,
                dead_code: true,
                warnings: false,
            },
        })
    );
}

module.exports = config;
