import { configure } from '@storybook/react';
import './../src/theme/app.less';

function loadStories() {
  require('../src/components/button/Button.story');
  require('../src/components/form/Input.story');
  require('../src/components/question-manager/Question.Story');
  require('../src/components/layout/Layout.story');
  require('../src/theme/global.story');
}

configure(loadStories, module);
