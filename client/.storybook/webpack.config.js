const webpackConfig = require('./../webpack.config')

module.exports = {
  plugins: [],
  module: {
    rules: webpackConfig.module.loaders
  },
};
